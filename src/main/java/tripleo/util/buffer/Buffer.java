/*
 * Elijjah compiler, copyright Tripleo <oluoluolu+elijah@gmail.com>
 *
 * The contents of this library are released under the LGPL licence v3,
 * the GNU Lesser General Public License text was downloaded from
 * http://www.gnu.org/licenses/lgpl.html from `Version 3, 29 June 2007'
 *
 */

package tripleo.util.buffer;

/**
 * Created 11/2/20 12:55 PM
 */
public interface Buffer {

    String getText();

    Buffer next();

    Buffer previous();

    Buffer parent();

}

//
//
//
