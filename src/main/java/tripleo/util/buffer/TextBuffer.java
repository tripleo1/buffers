/*
 * Tripleo-buffers, copyright Tripleo <oluoluolu+buffers@gmail.com>
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the LICENSE for the specific language governing permissions and
 *  limitations under the License.
 */
package tripleo.util.buffer;

public interface TextBuffer extends Buffer {

	void append(String string);

	void append_s(String string);

	void append_cb(String string);

	void decr_i();
	
	void incr_i();
	
	void append_nl_i(String string);

	void append_nl(String string);

	void append_ln(String string);

}