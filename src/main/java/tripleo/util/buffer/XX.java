/*
 * Tripleo-buffers, copyright Tripleo <oluoluolu+buffers@gmail.com>
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the LICENSE for the specific language governing permissions and
 *  limitations under the License.
 */

package tripleo.util.buffer;

import tripleo.util.NotImplementedException;

/**
 * @author Tripleo(acer)
 *
 */
public enum XX {
	SPACE, // ( " "),
	LPAREN, RPAREN, // ( ")"),

	COMMA // ( ",");
	, INDENT
	// String value;
	, LBRACE

	, RBRACE;

	//
	
	@Override
	public String toString() {
		return getText();
	}
	
	public String getText() {
		// TODO Auto-generated method stub
		if (this == SPACE) {
			return " ";
		} else if (this == LPAREN) {
			return "(";
		} else if (this == RPAREN) {
			return ")";
		} else if (this == LBRACE) {
			return "{";
		} else if (this == RBRACE) {
			return "}";
		} else if (this == COMMA) {
			return ",";
		} else {
			NotImplementedException.raise();
			return null;
		}
	}

}

//
//
//
