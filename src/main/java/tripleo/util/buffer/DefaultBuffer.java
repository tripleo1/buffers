/*
 * Tripleo-buffers, copyright Tripleo <oluoluolu+buffers@gmail.com>
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the LICENSE for the specific language governing permissions and
 *  limitations under the License.
 */
package tripleo.util.buffer;

/**
 * @author Tripleo(acer)
 *
 */
public class DefaultBuffer implements TextBuffer {

	private Buffer _parent;
	private Buffer _next;
	private Buffer _previous;

	public DefaultBuffer(String string) {
		append(string);
	}

	/* (non-Javadoc)
	 * @see tripleo.util.buffer.TextBuffer#append(java.lang.String)
	 */
	@Override
	public void append(String string) {
		text.append(string);
	}

	/**
	 * Append string with space
	 * 
	 * @see tripleo.util.buffer.TextBuffer#append_s(java.lang.String)
	 */
	@Override
	public void append_s(String string) {
		text.append(string);
		text.append(" ");
	}

	/**
	 * Append string with closing brace
	 * 
	 * @see tripleo.util.buffer.TextBuffer#append_cb(java.lang.String)
	 */
	@Override
	public void append_cb(String string) {
		text.append(string);
		text.append("}");
	}

	StringBuilder text = new StringBuilder();
	private int incr=0;
	
	@Override
	public void decr_i() {
		incr--;
	}
	
	@Override
	public void incr_i() {
		incr++;
	}
	
	@Override
	public void append_nl_i(String string) {
		// TODO Auto-generated method stub
		text.append(string);
		text.append("\n");
		doIndent();
	}
	
	private void doIndent() {
		text.append(new_String('\t', incr));
	}
	
	public static String new_String(final char c, int length) {
		StringBuilder s=new StringBuilder(length);
		while (length-->0) s.append(c/*('\t')*/);
		return s.toString();
	}

	@Override
	public void append_nl(String string) {
		text.append(string);
		text.append("\n");
		doIndent();
	}

	public void append_s(String string, XX sep) {
		text.append(string);
		text.append(sep.getText());
	}

	@Override
	public void append_ln(String string) {
		text.append(string);
		text.append("\n");
		doIndent();
	}

	@Override
	public String getText() {
		// TODO Auto-generated method stub
		return text.toString();
	}

	@Override
	public Buffer next() {
		return _next;
	}

	@Override
	public Buffer previous() {
		return _previous;
	}

	@Override
	public Buffer parent() {
		return _parent;
	}
}

//
//
//
