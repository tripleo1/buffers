/*
 * Tripleo-buffers, copyright Tripleo <oluoluolu+buffers@gmail.com>
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the LICENSE for the specific language governing permissions and
 *  limitations under the License.
 */
/**
 * 
 */
package tripleo.util.buffer;

import java.util.HashMap;
import java.util.Map;

//import tripleo.elijah.comp.GenBuffer;
//import tripleo.elijah.gen.nodes.ArgumentNode;

/**
 * @author Tripleo(ecl)
 *
 */
public class BufferSequenceBuilder {
	
	private int semi_eol_count = 0;
	private String _built;
	private Map<String, TextBuffer> parts;
	private Map<Integer, String> part_names;

	public BufferSequenceBuilder(int i) {
		// TODO Auto-generated constructor stub
		parts = new HashMap<String, TextBuffer>(i);
		part_names = new HashMap<Integer, String>(i);
	}

//	public BufferSequenceBuilder(int i, Iterable<ArgumentNode> argumentsIterator, Transform transform1,
//			XX comma, GenBuffer gbn) {
//		// TODO Auto-generated constructor stub
//		parts = new HashMap<String, TextBuffer>(i);
//		part_names = new HashMap<Integer, String>(i);
//		//
//		Bufbldr bb=new Bufbldr(gbn);
//		for (ArgumentNode an : argumentsIterator) {
//			transform1.transform(an, bb);
//			bb.append(comma.getText());
//		}
//		_built = bb.getText();
//	}

	public BufferSequenceBuilder named(String string) {
		// TODO Auto-generated method stub
		part_names.put(part_names.size(), string);
		return this;
	}

	public BufferSequenceBuilder semieol() {
		String key = "klkkl"+ (++semi_eol_count);
		// TODO Auto-generated method stub
		parts.put(key , new DefaultBuffer(";\n"));
		part_names.put(part_names.size()+1, key);
		return this;
	}

	public void set(String part_name, String setTo) {
		// TODO Auto-generated method stub
		parts.put(part_name, new DefaultBuffer(setTo));
	}

	public void set(String part_name, String setTo, char sep) {
		// TODO Auto-generated method stub
		parts.put(part_name, new DefaultBuffer(setTo+sep));
	}

	public BufferSequence build() {
		BufferSequence bsq = new BufferSequence();
		for (String s : part_names.values()) {
			bsq.add(parts.get(s));
		}
//		for (TextBuffer element : parts.values()) {
//			bsq.add(element);
//		}
		return bsq;
	}

	public void set(String part_name, String setTo, XX sep) {
		// TODO fix septoString
		parts.put(part_name, new DefaultBuffer(setTo+sep.toString()));
	}

	public void set(String part_name, TextBuffer sb2) {
		// TODO Auto-generated method stub
		parts.put(part_name, sb2);
	}

	public String fieldNamed(String string) {
		return parts.get(string).getText();
	}

	public boolean fieldIsSemiEol(int i) {
		return part_names.get(i).matches("klklkl[0-9]+");
	}

}
