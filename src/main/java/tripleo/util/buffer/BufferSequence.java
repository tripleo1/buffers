/*
 * Tripleo-buffers, copyright Tripleo <oluoluolu+buffers@gmail.com>
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the LICENSE for the specific language governing permissions and
 *  limitations under the License.
 */
/**
 * 
 */
package tripleo.util.buffer;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Tripleo
 *
 * Created 	Mar 31, 2020 at 7:07:40 PM
 */
public class BufferSequence {

	private List<TextBuffer> parts = new ArrayList<TextBuffer>();

	public void add(TextBuffer element) {
		parts.add(element);
	}

	public String getText() {
//		if (_built != null)
//			return _built;
//		//
		StringBuffer sb = new StringBuffer();
		for (TextBuffer element : parts) {
			sb.append(element.getText());
		}
		return sb.toString();
	}

}

//
//
//
