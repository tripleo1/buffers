/*
 * Tripleo-buffers, copyright Tripleo <oluoluolu+buffers@gmail.com>
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the LICENSE for the specific language governing permissions and
 *  limitations under the License.
 */
/**
 * 
 */
package tripleo.util.buffer;

/**
 * @author Tripleo
 *
 */
public class EnclosedBuffer extends DefaultBuffer {

	private String _payload;
	private String _right;
	private String _left;

	public EnclosedBuffer(String left, String right) {
		super("");
		// TODO Auto-generated constructor stub
		_left=left;
		_right=right;
	}

	public EnclosedBuffer(String left, XX right) {
		super("");
		_left=left;
		_right=right.getText();
	}

	public void setPayload(BufferSequenceBuilder sequence) {
		_payload = sequence.build().getText();
	}
	
	@Override
	public String getText() {
		// TODO Auto-generated method stub
		return //super.toString();
			String.format("%s%s%s", _left, _payload, _right);
	}

	public void setPayload(String string) {
		_payload = string;
	}
}

//
//
//
