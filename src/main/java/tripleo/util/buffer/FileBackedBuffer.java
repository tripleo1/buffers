/*
 * Tripleo-buffers, copyright Tripleo <oluoluolu+buffers@gmail.com>
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the LICENSE for the specific language governing permissions and
 *  limitations under the License.
 */
package tripleo.util.buffer;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * @author Tripleo(acer)
 *
 */
public class FileBackedBuffer implements TextBuffer {

	private Buffer _parent;
	private Buffer _next;
	private Buffer _previous;

	TextBuffer backing=new DefaultBuffer(""); // TODO bad api
	private String fn;

	@Override
	public void finalize() {
		dispose();
	}
	
	public void dispose() {
		try {
			final FileOutputStream fileOutputStream = new FileOutputStream(fn);
			fileOutputStream.write(backing.toString().getBytes());
			fileOutputStream.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public FileBackedBuffer(String fn) {
		this.fn=fn;
	}

	@Override
	public void append(String string) {
		backing.append(string);
	}

	@Override
	public void append_s(String string) {
		backing.append_s(string);
	}

	@Override
	public void append_cb(String string) {
		backing.append_cb(string);
	}

	@Override
	public void decr_i() {
		backing.decr_i();
	}
	
	@Override
	public void incr_i() {
		backing.incr_i();
	}
	
	@Override
	public void append_nl_i(String string) {
		// TODO Auto-generated method stub
		backing.append_nl_i(string);
	}

	@Override
	public void append_nl(String string) {
		// TODO Auto-generated method stub
		backing.append_nl(string);
	}

	@Override
	public void append_ln(String string) {
		// TODO Auto-generated method stub
		backing.append_ln(string);
	}

	@Override
	public String getText() {
		// TODO Auto-generated method stub
		return backing.getText();
	}


	@Override
	public Buffer next() {
		return _next;
	}

	@Override
	public Buffer previous() {
		return _previous;
	}

	@Override
	public Buffer parent() {
		return _parent;
	}
}
